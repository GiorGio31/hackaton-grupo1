FROM openjdk:15-alpine

ADD target/grupo1-0.0.1.jar app.jar

COPY starter.sh /starter.sh

RUN sh -c 'touch app.jar' && touch ./starter.sh && chmod +x ./starter.sh

ENTRYPOINT ["sh","starter.sh"]


