#!/bin/sh
java -jar 'app.jar' \
--server.port=${SERVERPORT} \
--spring.data.mongodb.host=${MONGOSERVER} \
--spring.data.mongodb.port=27017 \
--spring.data.mongodb.database=${MONGODB} \
--spring.data.mongodb.username=${MONGODBUSER} \
--spring.data.mongodb.password=${MONGOUSERPASS}