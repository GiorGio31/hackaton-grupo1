package com.hackatonpractitioner.grupo1.model;

public class Huella {

    private String vehiculo; //gasolina, diesel, electrico, no
    private String calefaccion; //butano, gas, carbon, no
    private Integer electricidad; //kwh al año
    private String dieta; //mediterranea, vegetariana, vegana

    public Huella(String vehiculo, String calefaccion, Integer electricidad, String dieta){
        this.vehiculo = vehiculo;
        this.calefaccion = calefaccion;
        this.electricidad = electricidad;
        this.dieta = dieta;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getCalefaccion() {
        return calefaccion;
    }

    public void setCalefaccion(String calefaccion) {
        this.calefaccion = calefaccion;
    }

    public Integer getElectricidad() {
        return electricidad;
    }

    public void setElectricidad(Integer electricidad) {
        this.electricidad = electricidad;
    }

    public String getDieta() {
        return dieta;
    }

    public void setDieta(String dieta) {
        this.dieta = dieta;
    }

    //Devuelve en un entero el valor de huella de carbono del usuario. Valor de 0 a 16
    public Integer calculaHuella(Huella huella){
        Integer total=0;
        switch(huella.getVehiculo()){
            case "no":
                break;
            case "electrico":
                total+=1;
                break;
            case "gasolina":
                total+=2;
                break;
            case "diesel":
                total+=3;
                break;
            default:
                total+=4;
        }
        switch(huella.getCalefaccion()){
            case "no":
                break;
            case "gas":
                total+=1;
                break;
            case "butano":
                total+=2;
                break;
            case "carbon":
                total+=3;
                break;
            default:
                total+=4;
        }
        if (huella.getElectricidad()<1000)
            total+=1;
        else if (huella.getElectricidad()>=1000 && huella.getElectricidad()<=2000)
            total+=2;
        else if (huella.getElectricidad()>2000)
            total+=3;
        else
            total+=4;
        switch(huella.getDieta()){
            case "vegana":
                total+=1;
                break;
            case "vegetariana":
                total+=2;
                break;
            case "mediterranea":
                total+=3;
                break;
            default:
                total+=4;
        }

        return total;

    }
}
