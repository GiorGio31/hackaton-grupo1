package com.hackatonpractitioner.grupo1.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "prestamos")
public class PrestamoModel {

    @Id
    private String id;
    @Value("${mock.iduser}")
    private String idusuario;
    private String nombre;
    private String apellido1;
    private String dni;
    private Double capital;
    private Huella huella;
    private Integer valorhuella;
    private Double tipo;
    private Integer plazo;
    private String estado;

    public PrestamoModel(String id, String idusuario, String nombre, String apellido1,
                         String dni, Double capital, Huella huella,
                         Integer valorhuella, Double tipo, Integer plazo, String estado) {
        this.id = id;
        this.idusuario = idusuario;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.dni = dni;
        this.capital = capital;
        this.huella = huella;
        this.valorhuella = valorhuella;
        this.tipo = tipo;
        this.plazo = plazo;
        this.estado = estado;

    }

    public PrestamoModel(PrestamoFrontModel prestamoFrontModel){
        //this.id = id;
        this.idusuario = UUID.randomUUID().toString();;
        this.nombre = prestamoFrontModel.getNombre();
        this.apellido1 = prestamoFrontModel.getApellido1();
        this.dni = prestamoFrontModel.getDni();
        this.capital = prestamoFrontModel.getCapital();
        this.huella = prestamoFrontModel.getHuella();
        this.valorhuella = this.huella.calculaHuella(this.getHuella());
        this.plazo = prestamoFrontModel.getPlazo();
        this.calculaTipo();
        this.calculaEstado();
    }
    public PrestamoModel(){}

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }
    public String getIdusuario() { return idusuario; }
    public void setIdusuario(String idusuario) { this.idusuario = idusuario; }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Huella getHuella() {
        return huella;
    }

    public void setHuella(Huella huella) {
        this.huella = huella;
    }

    public Integer getValorhuella() {
        return valorhuella;
    }

    public void setValorhuella(Integer valorhuella) {
        this.valorhuella = valorhuella;
    }

    public Double getTipo() {
        return tipo;
    }

    public void setTipo(Double tipo) {
        this.tipo = tipo;
    }

    public Integer getPlazo() {
        return plazo;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void calculaTipo(){
        Double t = 0.0;
        switch (this.plazo){
            case 3:
                if( this.getValorhuella() < 5)
                    this.setTipo(3.0);
                else if (this.getValorhuella() >= 5 && this.getValorhuella() < 10)
                    this.setTipo(4.0);
                else if (this.getValorhuella() >= 10)
                    this.setTipo(5.0);
                break;
            case 6:
                if( this.getValorhuella() < 5)
                    this.setTipo(6.0);
                else if (this.getValorhuella() >= 5 && this.getValorhuella() < 10)
                    this.setTipo(7.0);
                else if (this.getValorhuella() >= 10)
                    this.setTipo(8.0);
                break;
            case 12:
                if( this.getValorhuella() < 5)
                    this.setTipo(9.0);
                else if (this.getValorhuella() >= 5 && this.getValorhuella() < 10)
                    this.setTipo(10.0);
                else if (this.getValorhuella() >= 10)
                    this.setTipo(11.0);
                break;
            default:
                this.setTipo(11.0);
        }

    }
    public void calculaEstado(){
        if (this.getValorhuella() < 6)
            this.setEstado("concedido");
        else if (this.getValorhuella() >= 6 && this.getValorhuella() < 10)
            this.setEstado("pendiente");
        else
            this.setEstado("rechazado");
    }





}
