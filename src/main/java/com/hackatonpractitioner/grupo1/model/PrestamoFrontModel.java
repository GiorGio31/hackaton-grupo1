package com.hackatonpractitioner.grupo1.model;

import org.springframework.data.annotation.Id;

public class PrestamoFrontModel {


    private String nombre;
    private String apellido1;
    private String dni;
    private Double capital;
    private Huella huella;
    private Integer plazo;

    public PrestamoFrontModel(String nombre, String apellido1,
                         String dni, Double capital, Huella huella, Integer plazo) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.dni = dni;
        this.capital = capital;
        this.huella = huella;
        this.plazo = plazo;

    }
    public PrestamoFrontModel(){}


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Huella getHuella() {
        return huella;
    }

    public void setHuella(Huella huella) {
        this.huella = huella;
    }


    public Integer getPlazo() {
        return plazo;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }
}
