package com.hackatonpractitioner.grupo1.controller;

import com.hackatonpractitioner.grupo1.model.Huella;
import com.hackatonpractitioner.grupo1.model.PrestamoFrontModel;
import com.hackatonpractitioner.grupo1.model.PrestamoModel;
import com.hackatonpractitioner.grupo1.service.PrestamoService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/apihackaton/v1")
@CrossOrigin(origins = "+", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
public class PrestamoController {

    @Autowired
    PrestamoService prestamoService;

    //Devuelve el listado de prestamos
    /*
    @GetMapping("/prestamos")
    public List<PrestamoModel> getClientes() {
        return prestamoService.buscarTodos();
    }*/
    @GetMapping("/prestamos")
    public ResponseEntity<List<PrestamoModel>> getClientes() {
        List<PrestamoModel> l = prestamoService.buscarTodos();
        return new ResponseEntity<List<PrestamoModel>>(l,HttpStatus.FOUND);
    }


    //Devuelve en el body como un entero el valor de la huella dados los parámetros de Huella
    /*
    @PostMapping("/util/calculohuella")
    public Integer getValorHuella(@RequestBody Huella huella) {
        return huella.calculaHuella(huella);
    }*/
    @PostMapping("/util/calculohuella")
    public ResponseEntity<Integer> getValorHuella(@RequestBody Huella huella) {
        Integer h = huella.calculaHuella(huella);
        return new ResponseEntity<Integer>(h, HttpStatus.ACCEPTED);
    }

    //Devuelve la información de préstamo según el ID pasado
    @GetMapping("/prestamos/{id}")
    public Optional<PrestamoModel> getPrestamoId(@PathVariable String id){
        return prestamoService.buscarPorId(id);
    }

    //Crea un prestamo y lo introduce en base de datos
    @PostMapping("/prestamos")
    public ResponseEntity<PrestamoModel> postPrestamo(@RequestBody PrestamoFrontModel precibido){

        PrestamoModel prestamo = new PrestamoModel(precibido);
        return new ResponseEntity<PrestamoModel>(prestamoService.crear(prestamo), HttpStatus.CREATED);

    }
    /*
    @PostMapping("/prestamos")
    public PrestamoModel postPrestamo(@RequestBody PrestamoModel prestamo){
        return prestamoService.crear(prestamo);
    }
*/
    //Borra el prestamo según el Id pasado, si es que existe
    @DeleteMapping("/prestamos/{id}")
    public boolean deletePrestamo(@PathVariable String id) {
        if (prestamoService.existePorId(id))
        {
            prestamoService.borrarPorId(id);
            return true;
        }
        else
            return false;
    }





}
