package com.hackatonpractitioner.grupo1.service;

import com.hackatonpractitioner.grupo1.model.PrestamoModel;
import com.hackatonpractitioner.grupo1.repository.PrestamoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrestamoService {

    @Autowired
    PrestamoRepository prestamoRepository;

    //Buscar
    public List<PrestamoModel> buscarTodos() {
        return prestamoRepository.findAll();
    }
    //Buscar por Id
    public Optional<PrestamoModel> buscarPorId(String id){
        return prestamoRepository.findById(id);
    }
    //Crear
    public PrestamoModel crear(PrestamoModel cliente){
        return prestamoRepository.save(cliente);
    }
    //Borrar por ID
    public void borrarPorId(String id){
        prestamoRepository.deleteById(id);
    }
    //Existe por ID
    public boolean existePorId(String id){
        return prestamoRepository.existsById(id);
    }
    //Huella

}
