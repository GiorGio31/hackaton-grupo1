package com.hackatonpractitioner.grupo1.repository;

import com.hackatonpractitioner.grupo1.model.PrestamoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrestamoRepository extends MongoRepository<PrestamoModel, String> {
}