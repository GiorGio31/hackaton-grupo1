#!/bin/bash

# Get version from pom.xml
export VERSION=$(cat pom.xml | grep "<version>.*</version>" | head -1 |awk -F'[><]' '{print $3}')

# Kill and remove previous container if exists
docker kill grupo1
docker rm grupo1

# Remove previous version if exists
docker rmi $(docker images -aq grupo1)

# COMPILE VERSION
mvn clean install

# BUILD NEW VERSION
docker build -t grupo1:$VERSION .

# SAVE AND REMOVE NEW IMAGE
docker save -o grupo1.tar grupo1:$VERSION
docker rmi grupo1:$VERSION

# LOAD IMAGE (PREVENT INTERMEDIAL IMAGES)
docker load -i grupo1.tar

sed -i "s/VERSION=.*/VERSION='${VERSION}'/" .env

# CLEAN FOLDER
rm grupo1.tar
rm -rf./target
